Uhr
-------

Die Uhr ist Teil der Einlasssituation von 15000 Gray. Das Tutorial sozusagen.

Wird der Minutenzeiger auf die richtige, volle Uhrzeit gedreht, wird der nächste Cue (UHR_SOLVED) ausgelöst und das eigentliche Spiel kann beginnen.

Super nicht nur für den Einlass ins Life Adventure sondern auch für den Einstieg in die Welt der DIY Elektrotechnik. Wenn du an einer Stelle nicht weiter kommst empfehlen wir dir eine kurze Lektüre auf der ["getting started" Seite der Arduino Homepage](https://www.arduino.cc/en/Guide/HomePage).

* [Werkzeug](#markdown-header-werkzeug)
* [Bauteile](#markdown-header-bauteile)
* [Zusammenbauen](#markdown-header-zusammenbauen)
* [Code aufspielen](#markdown-code-aufspielen)
* [Testen](#markdown-code-testen)
* [Tweaken](#markdown-code-tweaken)

![An der Uhr gedreht](http://machinaex.de/wp-content/uploads/2013/08/uhr.jpg "Die Uhr")

##Werkzeug

* Lötwerkzeug
* Abisolierzange
* Cutter / Teppichmesser
* Sekundenkleber


##Bauteile

Die Entscheidende Komponente der Uhr ist ein Magnetkontakter, im Fachjargon auch *Reed Kontakter*. Ein Schalter der, je nach Bauart, geschlossen oder geöffnet wird, wenn er sich nahe genug an einem Magneten befindet.
Wir befestigen also einen Magneten an Minuten und Stundenzeiger und verbauen hinter dem Ziffernblatt zwei Magnet Kontakter. Wird die uhr auf eine bestimmte Uhrzeit gedreht, lösen die Magnet Kontakter aus und wir können das an die Zentrale Schnittstelle weiterleiten.

![Bauteile für die Uhr Abbildung](img/bauteile.png "Bauteile für die Uhr")
![Kleine Bauteile für die Uhr Abbildung](img/bauteile_klein.png "Kleine Bauteile für die Uhr")

* [Wemos Mikrocontroller](https://www.aliexpress.com/store/product/D1-mini-Mini-NodeMcu-4M-bytes-Lua-WIFI-Internet-of-Things-development-board-based-ESP8266/1331105_32529101036.html)
* Analoge Wanduhr
* 2x [Magnetkontakter](https://www.conrad.de/de/reed-kontakt-1-schliesser-200-vdc-140-vac-1-a-10-w-pic-ms-108-3-506968.html?fromReco=1&recoReferrerProduct=506965&recoType=similar)
(Reed Kontakt)
* 2 Kleine aber kräftige Magneten
* [Streifenraster Platine](https://www.conrad.de/de/platine-epoxyd-l-x-b-160-mm-x-100-mm-35-m-rastermass-254-mm-wr-rademacher-wr-typ-710-5-inhalt-1-st-529519.html)
* 2 10k Ohm Widerstände
* 2 Platinen Buchsenleisten im Rastermaß 2,54 mit jeweils 8 Buchsen
* 2 [Abgewinkelte Platinen Stiftleisten im Rastermaß 2,54 mit jeweils 8 Stiften](https://www.conrad.de/de/stiftleiste-standard-anzahl-reihen-1-polzahl-je-reihe-40-bkl-electronic-10120186-1-st-741385.html)
* farbige Jumper Kabel


##Zusammenbauen

Verlöte die Stiftleisten auf dem Wemos an der Seite des USB Anschlusses. So dass die Stifte auf der anderen Seite herausragen.
![wemos_1](img/wemos_stiftleiste.png)
![wemos_2](img/wemos_loeten.png)

Schneide dir zwei Streifenrasterplatinen in ca. der Größe 30mmx40mm und eine in der Größe 40mmx50mm zurecht.

Benutze einen Cutter oder eine schmale Feile um die Leiterbahnen der größeren Platine in der Mitte zu unterbrechen.

Verlöte die 8er Buchsenleisten parallel zu dieser Unterbrechung, so dass du das Wemos aufstecken kannst und eine Verbindung auf der Platine zu jedem Pin des Wemos besteht.

![wemos_platine_unten](img/unterbrechung.png)

Verlöte die 8er Stiftleisten links und rechts von der Buchsenleiste so dass die Stifte mit den Pins des Wemos verbunden sind.

![wemos_platine](img/platine_fertig.png)

Löte die beiden Reed Kontakte auf jeweils eine der übrigen Platinen quer zu den Kupferstreifen auf.

![reed_oben](img/reed_begin.png)
![reed_unten](img/reed_begin_rueck.png)

Füge den Widerstand an einem der Reed Pins hinzu.
Schneide für das Anlöten der Jumper Kabel ein Ende des Verbinders ab und Entferne etwas von der Isolierung.
![abisoliert](img/abisoliert.png "Jumper Kabel coupiert")

An jede Reedkontakt Platine müssen anschließend drei Jumper Kabel (female) gelötet werden. Ein Pin wird später mit dem 5V Pin des Wemos verbunden (rot). Der andere Pin, der mit dem Widerstand verbunden ist wird später mit einem Digital Pin des Wemos verbunden. An dem nicht mit dem Reed Kontakt verbundenen Beinchen des Widerstandes, wird später der Gnd Pin des Wemos angeschlossen (schwarz).

Um den 5V (rot) und Gnd (schwarz) Jumper der beiden Platinen miteinander verbinden zu können löten wir auf einer der Platinen noch zwei stifte an.

![schaltkreis_wemos](img/schaltkreis_wemos.png)

Verbinde den Kontakter für den Minutenzeiger (grün) mit Digital pin 0 (D0) und den Kontakter für den Stundenzeiger (gelb) mit Digital Pin 1 (D1)

![reed_1](img/reed_fertig_1.png)

![reed_2](img/reed_fertig_2.png)

Um die Reed Kontakte an der richtigen Stelle anzubringen markiere auf der Uhr Rückseite die äußerste Stelle an der der Minutenzeiger auf 12 und an der der Stundenzeiger auf 8 steht.

![markierungen](img/markiert.png)

Platziere nun die beiden Reedkontakte an den Markierungen. Der Reedkontakt für den Minutenzeiger sollte möglichst weit am äußeren Rand sein damit er nicht vom Magnet ausgelöst wird, der am Stundenzeiger befestigt ist.

Anschließend befestige (am besten mit Sekundenkleber) die Magneten an den Zeigern der Uhr. Beachte dabei dass die Magnete auf der Höhe der Reedkontakte auf der anderen Seite sein müssen wenn der Zeiger auf 8 Uhr steht bzw. der Minutenzeiger auf 12 Uhr.

![uhr_rueckseite](img/fertig_hinten.png)
![uhr_rueckseite_nah](img/fertig_hinten_nah.png)

Wenn deine Uhr wie diese hier aus Holz oder Karton besteht kannst du die Platinen an der Rückseite der Uhr verschrauben. Ansonsten versuche es mit kräftigem Klebeband.


##Code aufspielen

Wie du den WEMOS mit der Datei `uhr_wemos.ino` bespielst kannst du [hier](../README.md) im Abschnitt *Code aufspielen* nachlesen.

Öffne den Serial Monitor um den Code zu überprüfen.

![uhr_wemos_console1](img/uhr_wemos_console1.png "Verbindung wurde hergestellt")
![uhr_wemos_console2](img/uhr_wemos_console2.png "Reed Kontakte wurden ausgelöst")

##Testen

Mache den Aufbau ggf. zunächst auf dem Breadboard mit einem Arduino UNO.

![uhr_arduino](img/schaltplan_uhr_arduino.png "Da ist noch Platz für Experimente")

Öffne die Datei `uhr_arduino.ino` mit der Arduino IDE.

Wähle den Arduino USB Port unter
`Tools -> Ports` aus. 

Wähle unter `Tools -> Boards` `Arduino/Genuino Uno` aus und Spiele den Code auf.

Überprüfe über den Arduino Serial Monitor ob du ein Feedback bekommst, wenn du die Magnete an die Reed Kontakte hälst.

![uhr_arduino](img/uhr_arduino_console.png "Serial Console")


##Tweaken

Die Uhrzeit lässt sich natürlich ohne weiteres modifizieren indem du die Reedkontakte an einer anderen Stelle platzierst.
Mit mehr Reedkontaktern und einem größeren Mikrokontoller oder einem guten Multiplexing lässt sich die Uhrzeit natürlich genauer tracken.