/*
 * Dieser Sketch ist Teil der Bastelanleitung fuer das Real Live Game Adventure
 * 15000 Gray von machina eX.
 * 
 * Spiele ihn auf den Arduino oder Wemos fuer die Uhr
 * auf um sie in das Game zu integrieren.
 * 
 * Mehr Informationen und Anleitungen findest du hier:
 * https://www.machinaex.de/15000-gray-diy-kit/
 *
 * Ver.0.3 29.01.2018
 */

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

// Variablen fuer die Verbindung zum Netzwerk
const char* ssid = "deineWiFiID";
const char* password = "deinWiFiPasswort";

bool wifiConnected = false;


// Variablen fuer die Netzwerkkommunikation
IPAddress ip(192,168,178,112); // Die IP Adresse der Uhr
IPAddress gateway(192,168,178,1); // Die IP Adresse des Routers
IPAddress subnet(255,255,255,0); // Die Subnetzmaske. Die kann so bleiben
IPAddress outIP (192,168,178,51); // Die IP Adresse der Zentralen Schnittstellen Software

unsigned int localPort = 8888; // Der Kommunikationsport
WiFiUDP UDP;
bool udpConnected = false;
char packetBuffer[UDP_TX_PACKET_MAX_SIZE]; //Zwischenspeicher fuer eingehende Daten
char ReplyBuffer[] = "acknowledged"; // Bestaetigungsnachricht


// Variablen für die Magnetkontakter
bool reedA = false;
bool reedAstate = false;

bool reedB = false;
bool reedBstate = false;

int reedApin = D0;
int reedBpin = D1;

/*
* Wird bei Start einmal ausgefuehrt
*/
void setup() {
  // Serial Verindung herstellen
  Serial.begin(115200);
  Serial.println("Uhr verbindung wird hergestellt ...");

  // Wifi Verbindung herstellen
  wifiConnected = connectWifi();
   
  // ueberpruefen ob eine Verbindung hergestellt werden konnte
  if(wifiConnected)
  {
    udpConnected = connectUDP();
    if (udpConnected)
    {
        Serial.println("... verbindung hergestellt");
    }
  }

  // Digital pins auf dem WEMOS initialisieren
  pinMode(reedApin, INPUT);
  pinMode(reedBpin, INPUT);
}

/*
* Wird endlos wiederholt nachdem setup() durchgefuehrt wurde
*/
void loop() {

  // In Variable uebertragen ob eine Spannung an pin A oder B anliegt
  reedA = digitalRead(reedApin);
  reedB = digitalRead(reedBpin);
  
  // Kontakt A auf Aenderung ueberpruefen
  if(reedA != reedAstate)
  {
    Serial.print("Reedkontakt A: ");
    Serial.println(reedA);
    
    // Wenn beide Kontakte geschlossen sind, Nachricht ueber Netzwerk versenden
    if ( reedA == true && reedB == true )
    {
      Serial.println("Die Zeiger stehen auf 8 Uhr");
      sendUDPstr("solved", outIP);
    } else {
      Serial.println("Die Zeiger stehen NICHT auf 8 Uhr");
      sendUDPstr("not_solved", outIP);
    }
    
    reedAstate = reedA;
  }

  // Kontakt B auf Aenderung ueberpruefen
  if(reedB != reedBstate)
  {
    Serial.print("Reedkontakt B: ");
    Serial.println(reedB);

    // Wenn beide Kontakte geschlossen sind, Nachricht ueber Netzwerk versenden
    if ( reedA == true && reedB == true )
    {
      Serial.println("Die Zeiger stehen auf 8 Uhr");
      sendUDPstr("solved", outIP);
    } else {
      Serial.println("Die Zeiger stehen NICHT auf 8 Uhr");
      sendUDPstr("not_solved", outIP);
    }
    
    reedBstate = reedB;
  }
  

}
