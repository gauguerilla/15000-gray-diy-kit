# TIME BOMB 15 000 GRAY #

-- work in progress --
-- deutsche übersetzung folgt --

before you assemble the Bomb, take an Arduino UNO or a similar Microcontroller and install (techtalkers say "flash") the software for the Bomb on the Arduino.
This will help testing the hardware while assembly.

![Vorderansicht](img/front.jpg "vorderansicht")
(pic 2)

## DISPLAY ##

connect display via i2c protocol:
- connect i2c pins like in pic1 with jumper cables (orange left, yellow right pin as seen from backside of the displaypin --> orange is SCL, yellow is SDA within I2C standard)
- connect other side of the pins to arduino
    - in our case with an Arduino Uno:
        - SCL (orange) --> Arduino Pin A5
        - SDA (yellow) --> Arduino Pin A4
    - IGNORE unless you do not use an Arduino UNO:
        - *other Arduinos or other Microcontrollers will have other Pins as SCL/SDA. please check the specs of your Microcontroller in use!*
- connect power to the display:
    - like in pic1:  Red  Jumper Cable --> (+); Blue Jumper Cable --> (-)
    - Red (+) --> Arduino Pin 5V
    - Blue (GND) --> Arduino Pin GND

![](img/display2.jpg "")
(pic 1)


## PINS TO DEFUSE THE BOMB ##

- use 8 laboratory sockets like in pic2 and pic3 for the inputs of the cables. We preferred the ones like the red one for they were easier to solder but didn't have enough.

![](img/pins3.jpg "")
(pic 2)
![](img/pins2.jpg "")
(pic 3)

- drill holes into the case and put the sockets in, screw them really tight (they love to loosen over time, maybe a little glue helps in the end) and solder singular cables with jumper wires at the end like in pic 4 and pic 5

    - the pins on the side will become our OUTPUT Pins and therefore Arduino Pins D2,D3,D4,D5
    - viewed from the backside:
      - D2 --> upper right side (yellow)
      - D3 --> lower right side (blue)
      - D4 --> upper left side (green)
      - D5 --> upper right side (red)

![](img/pins1.jpg "")![](img/overview1.jpg "")
(pic 4 + 5)

- The pins of the wires will go to Arduino Pins D6,D7,D8,D9
like shown in pic6
- viewed from the backside:
  - D6 --> far left
  - D7 --> center left
  - D8 --> center right
  - D9 --> far right

![](img/pinsArduino.jpg "")
(pic 6)

- with all inputs and outputs the Arduino UNO should look like pic7 (yes, we could have done a better job with the color coding... maybe you can manage to match the colors of input 1 and output 1 and so forth.... ;-P )

![](img/pinsArduino2.jpg "")
(pic 7)

![](img/frontPinNums.jpg "")
(pic 8)

![](img/overview2.jpg "")
(pic 9)



the bomb can now be defused. Yehaaa!

## SOUND FROM INSIDE OF THE Bomb

-- tbd --

## NETWORK CONTROL via WEMOS D1 and Serial2Serial

To make the Bomb talk with the Rest of the system via WiFi, we need to connect it to a WEMOS D1 mini (or some other ESP8266 Wifi enabled Microcontroller). <br>
While the Arduino Uno takes care of all the logic within the bomb, the WEMOS D1 mini will send and receive all commands and events from and to the bomb. But in order to be able to do that, the WEMOS D1 mini has to be able to talk to the Arduino UNO first:

- connect to jumper cables from the Arduino Pins "GND" and "Vin" to the WEMOS D1 mini Pins GND (on some boards just "G") and "5V" like in pics 10-12. *(10 and 11 show our test setup on a breadboard, 12 shows our final setup in which we soldered the pins to the WEMOS directly)*

  ![](img/wemos1.jpg "")![](img/wemos2.jpg "")
  (pic 10 + 11)
  ![](img/wemos4.jpg "")
  (pic 12)

- then install all dependencies (the stuff you need to install before using sth) for the WEMOS D1 mini by following the official documentation: https://wiki.wemos.cc/tutorials:get_started:get_started_in_arduino



- install (flash) the code from this repository called "WEMO_serial_wifi_OSC_router.ino" onto the WEMOS D1 mini. (*beginner: you probably want to use the Arduino IDE for that, because that way is documented best*)
  - before go to line 15, 16 put in your WiFis ssid (*it's name*) and password. At line 22-25 put in the IP the bomb should have in your network (ip) the routers IP address (gateway), the address the bomb should talk back to (outIP) into the code and save. You can probably leave the subnet variable as it is.


now the bomb can be talked to and answer via WiFi. Yeahhhh!!!

### Network Commands (UDP Strings)###

It understands the following commands when send as Strings via UDP/OSC to its IP on Port 8888:

`/TIME <num>` number to be displayed on the bombs display.
- e.g. `/TIME 0245` will make the display show 2 minutes 45 seconds.

`/ECHO` bomb will answer with the current displayed time and the
configuration of the cables in its' inputs.

- example response: `/ECHO 0233 0003` : 2 minutes 33 seconds displayed on the bomb. only one cable in the last input socket. In this case: Cable number 3.

`/STANALONE <0-1>` Standalone Mode (the clock ticks on it's own) off/on. *default:* on

`/START` start the timer of the bomb (*only relevant if standalone mode on*)

`/STOP` stop the timer of the bomb (*only relevant if standalone mode on*)

`/SPEED <num>` with the speed of the display clock in millisecs
- e.g. `/SPEED 1000` for a update of the clock every second.
*only relevant if standalone mode is on (see above)*

`/FORWARD` direction on the clock counting (*only relevant if standalone mode on*)

`/BACKWARD` direction of the clock counting (*only relevant if standalone mode on*) *default*

`/SOUND <0-1>` Sound off/on



## LICENCE ##

Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)

You are free to:
Share — copy and redistribute the material in any medium or format
Adapt — remix, transform, and build upon the material
The licensor cannot revoke these freedoms as long as you follow the license terms.

for commercial use please contact philip(at)machinaex.de
