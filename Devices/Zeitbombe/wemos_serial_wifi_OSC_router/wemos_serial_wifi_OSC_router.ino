#include <ESP8266WiFi.h>
#include <WiFiUDP.h>
// OSC/UDP Setup // as variant if you want to use OSC packages instead of pure UDP
//#include <OSCBundle.h>
//#include <OSCMessage.h>

// Serial receive vars --> these will be send via OSC
char receivedChar;
const byte numChars = 64; // max number of chars that can be read //used to be 32
char receivedChars[numChars]; // an array to store the received data
boolean newData = false;
 
//wifi connection variables
///*
const char* ssid = "deinwifi";
const char* password = "deinwifipasswort";

boolean wifiConnected = false;


// UDP variables
IPAddress ip(192,168,43,111); // the IP of the Bomb
IPAddress gateway(192,168,43,1); // your routers IP
IPAddress subnet(255,255,255,0); // you probably won't need to change this
IPAddress outIP (192,168,43,175); // your adress / the adress to send to

unsigned int localPort = 8888; // the port for in and out
WiFiUDP UDP;
boolean udpConnected = false;
char packetBuffer[UDP_TX_PACKET_MAX_SIZE]; //buffer to hold incoming packet,
char ReplyBuffer[] = "acknowledged"; // a string to send back

//char* address = ("/BOMB");  // OSC address identifying msgs coming from the bomb

//##############################################################
//###################### INI ##################################

void setup() 
{
  // Initialise Serial connection
 Serial.begin(9600);
// Serial1.begin(9600); // depending on the type of board and what hardware serials it has you might need Serial1
 Serial.println("wemos ready");
  
  // Initialise wifi connection
  wifiConnected = connectWifi();
   
  // only proceed if wifi connection successful
  if(wifiConnected)
  {
    udpConnected = connectUDP();
    if (udpConnected)
    {
        // nothing here. but could be.
    }
  }
}

//##############################################################
//###################### MAIN ##################################

void loop() 
{
  //receiveOSC();
  receiveUDP();
  recvWithEndMarker();
  showNewData();
  
  delay(10);
}

//##############################################################
//#################### FUNCTIONS ###############################

//##############################################################
// connect to UDP – returns true if successful or false if not

boolean connectUDP()
{
    boolean state = false;
    
    Serial.println("");
    Serial.println("Connecting to UDP");
    
    if(UDP.begin(localPort) == 1)
    {
      Serial.println("Connection successful");
      state = true;
    }
    else
    {
      Serial.println("Connection failed");
    }
    
    return state;
}

//##############################################################
// connect to wifi – returns true if successful or false if not

boolean connectWifi()
{
    boolean state = true;
    int i = 0;
    WiFi.config(ip,gateway,subnet);// optional if we want to get an IP of our choice if possible
    WiFi.begin(ssid, password);
    Serial.println("");
    Serial.println("Connecting to WiFi");
    
    // Wait for connection
    Serial.print("Connecting");
    while (WiFi.status() != WL_CONNECTED) 
    {
      delay(500);
      Serial.print(".");
      if (i > 10){
      state = false;
      break;
      }
      i++;
    }
    if (state)
    {
      Serial.println("");
      Serial.print("Connected to ");
      Serial.println(ssid);
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
      
      char myIp[24];
      sprintf(myIp, "%d.%d.%d.%d",ip[0],ip[1],ip[2],ip[3]);
      
          // send hello I'm there to the home base:
          char hello[48];
          sprintf(hello, "online as:%s:%d", myIp,localPort);
          sendUDPstr(hello, outIP);
          /*OSCMessage msg(address);
          msg.add("online as: ");
          msg.add(myIp);
          UDP.beginPacket(outIP, localPort);
          msg.send(UDP);
          UDP.endPacket();
          msg.empty();
          */
       //
    }
    else 
    {
      Serial.println("");
      Serial.println("Connection failed.");
    }
    return state;
}

//##############################################################
// receive UDP input and forward


void receiveUDP()
{
  // if there’s data available, read a packet
  int packetSize = UDP.parsePacket();
  if(packetSize)
  {
    // read the packet into packetBufffer
    UDP.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    
    inputhandler(packetBuffer); //forward UDPinput into Serial
    for(int i=0;i<UDP_TX_PACKET_MAX_SIZE;i++) packetBuffer[i] = 0; //empty the buffer again
  }
  
}

/*
void receiveOSC()
{
  OSCMessage oscmsg;
  int size;

  if (( size = UDP.parsePacket()) > 0)
  {
    while (size--)
    {
      oscmsg.fill( UDP.read() );
    }

    if (!oscmsg.hasError())
    {
      oscmsg.route(address, OSChandler); // forward OSCinput into Serial
     
    } 
    else  
    {
      Serial.println("MESSAGE ERROR!");
    }
  }
}
*/

//###########################################################
// handleInputs

void inputhandler(String msg)
{
    // <-- here we could add prefilters, if we'd need them

    Serial.println(msg); // send incoming messages into Serial without changes
    sendUDPstr("got message and forwarded it to Serial", outIP); // report back in pure UDP
    //sendOSCstr(address,"got OSC message and forwarded it to Serial", outIP); // report back in OSC
  
}
/*
void OSChandler(OSCMessage &msg, int offset)
{
  //Serial.print("Incomming > ");
  if ( msg.isString(0) )
  {
    int length = msg.getDataLength(0);

    char strmsg[length];
    msg.getString(0, strmsg, length);

    Serial.println(strmsg);
    sendOSCstr(address,"got OSC message and forwareded it to Serial", outIP);  
  }
}
*/

//###########################################################
// send UDP / OSC

void sendUDPstr(char* message, IPAddress outIPInput)
{
  UDP.beginPacket(outIPInput, localPort);
  UDP.write(message);
  UDP.endPacket();
}

/*
void sendOSCstr(char* OSCaddress, char* message, IPAddress outIPInput)
{
  OSCMessage msg(OSCaddress);
  msg.add(message);
  UDP.beginPacket(outIPInput, localPort);
  msg.send(UDP);
  UDP.endPacket();
  msg.empty();
}
*/

//##############################################################
// check Serialinput for new Data

void recvWithEndMarker() 
{
   static byte ndx = 0;
   char endMarker = '\n';
   char rc;
   
  
  while (Serial.available() > 0 && newData == false) 
  {
     rc = Serial.read();
    
     if (rc != endMarker) 
     {
       receivedChars[ndx] = rc;
       ndx++;
      if (ndx >= numChars) 
        {
          ndx = numChars - 1;
        }
      }
      else 
      {
         receivedChars[ndx] = '\0'; // terminate the string
         ndx = 0;
         newData = true;
      }
   }
}

//##############################################################
// send data from Serial to Network via OSC

void showNewData() {
 if (newData == true) {
  sendUDPstr(receivedChars, outIP);
   //sendOSCstr(address,receivedChars,outIP);
   newData = false;
 }
}
