//import java.awt.geom.*;

public class Parser
{int numCmd;
  int cmdID;

  boolean errorDisplayed;
  boolean error;
  boolean cmdSent;
  boolean nounSent;
  boolean lastLetter;
  boolean secondWord;

  String spaceLess;
  String inputString;
  String tempLine;
  String compareLine;

  String[] commands;
  String[] nouns;

  Parser()
  {
    numCmd = 10;

    commands = new String[numCmd];
    commands[0] = "cd";
    commands[1] = "run";
    commands[2] = "copy";
    commands[3] = "dir";
    commands[4] = "help";
    commands[5] = "cd..";
    commands[6] = "a:"; // HAS to be lower case
    commands[7] = "b:";
    commands[8] = "d:";
    commands[9] = "c:";
    
    

    tempLine = "";

    cmdSent = false;
    nounSent = false;
    lastLetter = false;
    secondWord = false;

    cmdID = numCmd + 1;//int vars are zero when set up, so this a custom null fill

  }
  void sendParse(String input)
  {
    //convert input to lower case and remove directory
    inputString = input.toLowerCase().substring(fullDirectory.length(), input.length());
    spaceLess = inputString.trim();
   
    
    for (int i = 0 ; i < inputString.length(); i++)
    {
      char outLetter = inputString.charAt(i);
      if (i == inputString.length()-1)
      {
        lastLetter = true;
        charChecker(outLetter); 
      } 
      else {
        charChecker(outLetter);
      }
    }
  }

  void charChecker(char inLetter)
  {
    if (lastLetter == true)
    {
      if (inLetter != 32)
      {
        tempLine = tempLine + inLetter;
      }
      cmdNounSplitter();
      lastLetter = false;
    } 
    else {
      switch(inLetter)
      {
      case 32: // space
        cmdNounSplitter();
        break;
      case 8: //backspace
      case 10: //enter pc
      case 13: // enter mac
      case 65535:
      case 127:
      case 27:
        break;
      default:
        tempLine = tempLine + inLetter;
        //print(tempLine);
        break;
      }
    }
  }

  void resetError()
  {
    cmdID = numCmd + 1; // custom null statement
    cmdSent = false;
    errorDisplayed = false;
    compareLine = "";
  }

  void cmdNounSplitter()
  {
    if (tempLine.equals("") == false)
    {
      if (cmdSent == false)
      {
        cmdSent = true;
        nounCmdChecker(tempLine,true);
      } 
      else {
        nounCmdChecker(tempLine,false);
      }
    }
  }

  void nounCmdChecker(String inputLine, boolean cmdTrue)
  {

    if(cmdTrue == true)
    {

      for (int i = 0; i < numCmd; i++)
      {
        if(inputLine.equals(commands[i]))
        {
          cmdID = i;
        }
      }
      println(cmdID);
      if (cmdID == numCmd + 1)// this is my equivelent of null
      {
        println("bull");
        error(inputLine,false);// error if first statement is bull
      }

    } 
    else {
      cmdSent = false;
      //println("error check");
      if (errorDisplayed == false)
      {
        switch(cmdID)
        {
        case 0: // cd
          println("change directory " + "\"" + inputLine + "\"");
          break;
        case 1: // run
          println("run file " + "\"" + inputLine + "\"");
          break;
        case 2:// copy
          println("copy file" + "\"" + inputLine + "\"");
          break;          
        case 3: // [dir] error, dir does not take statements
          //println("error sent");
          error(inputLine,true);
          break;
        case 4:// [help] error, help does not take statements
          error(inputLine,true);
          break;
        case 5:// [cd..]
          println("cd.. with extra");
          error(inputLine,true);
          break;
        case 6:// A:
        println("A:TEST");
          error(inputLine,true);
          break;
        case 7:// B:
        println("B:TEST");
          error(inputLine,true);
          break;
        case 8:// D:
          println("D:TEST");
          error(inputLine,true);
          break;
        case 9:// C:
          println("C:TEST");
          error(inputLine,true);
          break;
         
        }
      }
    }
    compareLine = inputString.trim();
    println(compareLine + " " + commands[1] + " " + cmdID);
    if(commands[0].equals(compareLine) || commands[1].equals(compareLine) || commands[2].equals(compareLine))
    {
      error(inputLine, true);  
    }
    if(cmdID == numCmd + 1)
    {
      error(inputLine, false);
    }else if(cmdID > 2)
    {
              println("WOOHOOO");
              println("command 4 is " + commands[4]);
      //compareLine = inputString.trim();
      if (compareLine.equals(commands[3]) == false && compareLine.equals(commands[4]) == false && compareLine.equals(commands[5]) == false && compareLine.equals(commands[6]) == false && compareLine.equals(commands[7]) == false && compareLine.equals(commands[8]) == false && compareLine.equals(commands[9]) == false)
      {
        error(inputLine, true);
      } 
      else
      {
        execute.oneWord(cmdID);
        println(commands[cmdID] + " execute");
      }
    } else if(tempLine.equals(commands[cmdID]) == false)
    {
      execute.twoWords(cmdID, tempLine);
    }
    tempLine = "";
  }

  void error(String errorLine, boolean oneWord)
  {
    if(errorDisplayed == false)
    {
      if(cmdID == numCmd + 1)
      {
        textEngine.addString("error " + "\"" + inputString + "\"" + " is not a valid statement");
      }
      else if(cmdID > numCmd - 6)
      {
        textEngine.addString("error " + commands[cmdID] + " does not take parameters");
      }     
      else if (oneWord == true)
      {
        textEngine.addString("error " + commands[cmdID] + " needs a valid parameter");
      } 
        errorDisplayed = true;
    }
  }
}
