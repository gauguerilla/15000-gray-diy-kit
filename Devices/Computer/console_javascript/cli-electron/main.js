const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

var net = require('net'); // for tcp
var dgram = require('dgram'); // for udp

var udpPort = 8888;
var tcpPort = 8887;

startTCPServer();
startUDPServer();


////////////////////////////////////////////////////
////// STARTING AN APP WITHIN A WINDOW >>> /////

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    height: 600,
    width: 800
  })

  let url = require('url').format({
    protocol: 'file',
    slashes: true,
    pathname: require('path').join(__dirname, 'index.html')
  })
  mainWindow.loadURL(url)

})

//////////////////////////////////////////////////
///// <<< STARTING AN APP WITHIN A WINDOW ////////////:

function handleAppcommands(data)
{
  sendToRenderer(data);
}

electron.ipcMain.on('fromRenderer', (event, arg) => {

      console.log(config.name + "> " + arg)
 	}
)

/* Function: sendToRenderer
*  Forward message to app script
*/
function sendToRenderer(message)
{
	mainWindow.webContents.send('fromMain', message);
}


///////////////////////////////////////
/// Network Receive Servers to receive messages from the AppHandler:

function startUDPServer()
{
	var port = udpPort;
	var host = '0.0.0.0';
	var server = dgram.createSocket('udp4');
	server.on('listening', function(){
		var address = server.address();
		console.log("UDP server listening on "+ address.address +":"+port)
	});
	server.on("message", function (message, remote) {
		console.log("UDP: " + remote.address + ":" + remote.port + " " + message);

		message = String(message);
		message = handleAppcommands(message);

		mainWindow.webContents.send('fromMain', message);
	});
	server.bind(port, host);
}



function startTCPServer()
{
	var server = net.createServer();
	server.on('connection', handleConnection);

	server.listen(tcpPort, "0.0.0.0", function() {
	  console.log('TCP server listening to %j', server.address());
	});

	function handleConnection(conn) {
	  var remoteAddress = conn.remoteAddress + ':' + conn.remotePort;
	  console.log('new TCP client connection from %s', remoteAddress);

	  conn.setEncoding('utf8'); // important

	  conn.on('data', onConnData);
	  conn.once('close', onConnClose);
	  conn.on('error', onConnError);

	  function onConnData(message) {
	    //console.log('TCP: %s: %j', remoteAddress, message);
	    message = JSON.parse(message);
	    message = handleAppcommands(message);

	    // dispatch netReceive event for other parts of the program to know about it
	    if (message != undefined)
	    {
	    	mainWindow.webContents.send('fromMain', message);
	    }

	    // Evaluate message if contains target list
	    if (message.targets != undefined)
	    {
	    	conn.write("success"); // respond to validate
	    } else {
	    	conn.write("error");
	    }
	  }

	  function onConnClose() {
	    // console.log('connection TCP from %s closed', remoteAddress);
	  }

	  function onConnError(err) {
	    console.log('Connection TCP %s error: %s', remoteAddress, err.message);
	  }
	}
}
