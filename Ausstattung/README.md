Ausstattung
===========

15000 Gray ist, gemessen an anderen machina eX Produktionen oder Bühnenstücken, ein absolutes Leichtgewicht. Wenn man es geschickt anstellt lässt sich das gesamte Stück (Personal einmal ausgenommen) in einem PKW transportieren.

Trotzdem kommt hier einiges an Dingen zusammen, die eine wesentliche Rolle im Zusammenspiel der verschiednen Elemente in 15000 Gray haben.

* [Bühnenaufbau](#markdown-header-buhnenaufbau)
* [Requisiten](#markdown-header-requisiten)
* [Dokumente](#markdown-header-dokumente)
* [Kostüme](#markdown-header-kostume)


Bühnenaufbau
------------

Dieser (noch sehr skizzenhafte) Bühnenplan zeigt an, wo was in etwa stehen sollte. Wo ihr welchen Tisch genau platziert hängt natürlich von eurem Spielort ab. Beachtet, dass jeder Tisch eine Einheit mit den darauf oder daneben platzierten Requisiten, Dokumenten und der Beleuchtung bildet, die zusammen arrangiert werden sollten.

![Bühnenplan](stageplan.png "Skizze des Aufbaus")

Damit 15000 Gray unkompliziert durch die Lande Transportiert werden kann, besteht der Bühnenaufbau zu großen Teilen aus gewöhnlichen Tischen und Stühlen.

Der Aufbau hat also die folgenden Anforderung an den Spielort:

* 4 - 6 Tische mit mindestens 1qm Tischplatte.
* 4 - 6 Stühle
* 1 Beistelltisch/Kaffeetisch

Requisiten
----------

Die Liste der Dinge, die bei 15000 Gray von machina eX zum Einsatz kommen werden wir in naher Zukunft vervollständigen und genauer erläutern und ggf. bebildern. Zunächst findest du hier eine Übersicht was alles im Set zu finden ist.

Viele Gegenstände, die zur Ausstattung von 15000 gray gehören sind zunächst dazu da, eine Atmosphäre zu schaffen, das Labor als solches und die Welt der 15000 Gray Fiktion erlebbar zu machen.

###Nicht rätselrelevante Requisiten

* Handschellen
* Radio
* Kaffeemaschine
* Kaffeetasse
* Kaffelöffel
* Schale mit Äpfeln
* Laborequippment
	* Messbecher
	* Labor Schutzbrillen
	* Gummihandschuhe
	* Erlenmeyer Kolben
* Portrait von Marie Curie
* zusätzliche alte Monitore
* 2 Aktentaschen
* Alter Drucker
* Gameboy
* Alter Standkalender
* Dokumentenablagen
* Kleintier Trinkvorrichtung
* Dartscheibe
* Ledertasche mit: Bilderrahmen, Damenslip, Dartpfeil, Reagenzrohr, Stifte, Stoffhandschuhe, Taschnrechner, Notibuch
* Tacker
* Hinweisschild "Hauptschalter"
* Hinweisschild "Aufzug"
* Hinweisschild "Radioaktiv"
* Schreibtischuhr Faltblattanzeige
* Schreibmaschine

Einige der Requisiten spielen zudem eine wichtige Rolle in den [Taskscenes](../Script/#markdown-header-taskscene). Sie sind wichtig zum Lösen der Rätsel oder Teile der Rätsel indem sie als Eingabestelle fungieren oder den Performern als Requisit dienen mit dem sie auf eine Eingabe der Spieler reagieren. 

###Rätselrelevante Requisiten

* [Uhr](../Devices/Uhr)
* [Telefon](../Devices/Telefon)
* [Numpad](../Devices/Numpad)
* [Computer](../Devices/Computer)
* Monitor
* Alte Tastatur
* Computercase mit Diskettenlaufwerk
* Disketten
* Reagenzglas
* Reagenzglas Ständer
* Petrischale
* Pipette (keine Eingabestelle)
* Verdorrte Zimmerpflanze
* Pistolenkisten
* Besen
* Pistolen (keine Eingabestelle)
* Kleiderständer mit Kleiderhaken (keine Eingabestelle)
* [Bombe](../Devices/Zeitbombe)

Dazu zählen natürlich auch solche, die Hinweise zum Lösen der Rätsel oder Hinweise über Zusammenhänge geben. Diese sind im folgenfden Abschnitt [Dokumente](#markdown-header-dokumente) erläutert und aufgelistet.

Dokumente
----------

15000 Gray enthält, wie alle Live Games von machina eX, eine große Anzahl Dokumente die zusätzliche Informationen zur Erzählung liefern und ggf. Hinweise oder Erläuterungen zu Rätseln enthalten. Das sind Requisiten die etwa mit Texten oder Grafiken beschriftet oder bedruckt sind. 

Manche Texte und Grafiken findest du bereits im [Dokumente](Dokumente/) Ordner. Reproduziere die Dokumente indem du ggf. Texte ausdruckst oder auf dem entsprechenden Material abschreibst.

###Rätselrelevante Dokumente

* Klemmbrett
* Karteikarte
* Post its
* Postkarte
* Bilderrahmen Segelboot
* [Whiteboard](Dokumente/Whiteboard)
* Brief vom Hausmeister
* [Rezeptformel](Dokumente/Mischrätsel)
* Ringbuch
* Mäusekäfige mit angebissenem Apfel
* Notizbuch Amanda

###Nicht rätselrelevante Dokumente

* Karteikarten
* Fotos
* Post its
* Wissenschaftliche Bücher
* [Aktenordner](Dokumente/Aktenordner)

Kostüme
-------

Hier ergänzen wir in naher Zukunft eine Liste der Kostüme, wie sie in 15000 Gray zum Einsatz kommen.