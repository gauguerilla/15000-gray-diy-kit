SKRIPT 15000 GRAY - DIY KIT
===========================

* [Personen](#markdown-header-personen)
* [Kurzexposé](#markdown-header-kurzexpose)
* [Glossar](#markdown-header-glossar)
* [Das Skript](#markdown-header-das-skript)

PERSONEN
--------

**_performende Figuren_**

HANS HÖVEL, Professor, der eine wichtige Entdeckung gemacht hat, das Forschungsprojekt ist sein Liebling und er will es allein zu Ende bringen, damit die Konkurrenz nicht davon erfährt, bevor er verhindern kann, dass die Ergebnisse zu falschem Zweck genutzt werden

MONIKA MC LAIN, Assistentin/Doktorin 1, ist eine Spionin einer apokalyptischen Sekte im Projekt des Professors, sie will an die Forschungsergebnisse heran kommen und stellt sich im Spiel als die Böse dar, damit er zu ihrer Kollegin Vertrauen gewinnen kann

AMANDA AARON, Assistentin/Doktorin 2, ist ebenfalls eine Spionin im System, spielt aber die Rolle der Guten, um das Vertrauen des Professors zu gewinnen —> die Verteilung der beiden Rollen wird mittels Eingabe des Revolverrätsels von den SpielerInnen bestimmt

HOST/HOSTESSE, Einlasspersonal, das eine Einführung gibt (Cutscene 0) und den ersten Task performt (Taskscene 0). Ist nicht Teil der Erzählung

**_Hörspielfiguren_**

JÖRG JALOUX, Chef von Amanda und Monica und hohes Tier innerhalb der apokalyptischen Sekte. Ist an der Organisation eines atomaren Rundumschlags beteiligt, der eine neue Weltordnung iniziieren soll. Dazu werden aber die Forschungsergebnisse des Professors gebraucht.

RADIOMODERATOR_IN, Interviewt den Professor. Für den Fortgang der Handlung nicht von Bedeutung.

**_weitere Figuren_**

HELGA HÖVEL, Frau des Professors

 

KURZEXPOSÉ
-----------

Labor - Forscherteam

Professor Hövel hat im Rahmen seiner Forschung ein Ergebnis gefunden, eine gesamtgesellschaftlich wichtige Information. Seine beiden Mitforscherinnen sind eigentlich von der Konkurrenz, einer sektiererischen Organisation, die der Neuentdeckung des Professors habhaft werden wollen. Erfolglos und vom Chef unter Druck gesetzt, greifen die beiden zu Plan B.

Die eine der beiden Assistentinnen schlüpft in die Rolle der Erpresserin des Professors. Dazu hat sie Hövel eine Bombe um den Bauch gebunden. In der folgenden Cutscene stellt die Spionin ein Ultimatum, sie behauptet alle sterben zu lassen, wenn der Professor die Informationen nicht raus rückt. So unter Druck gesetzt, gibt er nach einem moralischen Appell den Ort der Forschungsergebnisse an die vermeintlich Gute der beiden Mitarbeiterinnen weiter, die bis dahin Ihre Kollegin mit einer Schusswaffe in Schach gehalten hatte. Die Farce wird aufgelöst und die beiden Frauen verlassen samt Ergebnis den Raum. Es ist an den Spielern die [Bombe](../Devices/Zeitbombe) zu entschärfen und den Professor vor dem Untergang zu retten.

**GROSSE ERFINDUNG**

Dem Professor ist es geglückt, das [Radiodurans-Bakterium](http://de.wikipedia.org/wiki/Deinococcus_radiodurans) (hält bis zu 15000 Gray aus) zu isolieren und in das Genom der Mäuse zu integrieren. Damit hat er - im Endeffekt - einen Impfstoff gegen Radioaktivität erfunden. Die Konkurrenz will nur bestimmten Menschen diesen Impfstoff zugänglich machen und plant eine atomare Sintflut zur Herstellung einer neuen Weltordnung.

In der letzten Nacht hat der Professor einen Fortschritt gemacht, eine Maus erfolgreich infiziert. Er hat keine Ahnung, warum es funktioniert hat. Die Assistenten denken aber, er habe eine Variable am Experiment geändert Es stellt sich jedoch mit der Lektüre der entsprechenden Dokumente heraus, dass die Maus scheinbar eine besondere Genomanomalie hat, die es nun näher zu erforschen gilt. Die Assistenten nehmen die entsprechenden Aufzeichnungen mit. Zuvor konnte sich die Spielergruppe aber entscheiden, ob sie die zielführenden oder die falschen Ergebnisse auf eine Diskette speichert - Paulas oder Ronnys - und damit den Ausgang der Story bestimmen…

GLOSSAR
-------

CUTSCENE	

Ist eine Spielsequenz, die nicht durch die Handlungen der Spieler unterbrochen werden kann. Sie vollzieht sich automatisch und gleicht im Prinzip einer Videoeinspielungen im Modus des Theaters.

TASKSCENE 	

Sind interaktive Sequenzen, in welchen das Handeln der Spieler gefragt ist. Die Handlung steckt fest und muss durch das Bewältigen einer oder mehrerer 	Aufgaben überbrückt werden. Die Performer agieren im Loop und _hinten_, um die Spieler mit Hinweisen beim Lösen der Rätsel zu unterstützen. Die Performer reagieren indirekt auf die Handlungen der Spieler. In diesem Script sind die Taskscenes zur Übersichtlichkeit grau hinterlegt.

LOOP	

Ein sich in einer Schleife befindender Bewegungsablauf der Performer während einer Taskscene. Er besteht aus einer kleinen Sequenz mit einem deutlich markierten Anfang (etwa einer bouncenden Bewegung). Rätselaufgaben sind mit jeweils einem oder mehreren Loops belegt.

HINTING	

Die besondere Informationsvergabe der Performer während einer Taskscene/eines Loops. Die Hints bestehen aus Hinweisen, die die Spieler mit Informationen zur Rätsellösung versehen. Sie sind oftmals gestaffelt, in dem sie entweder mit der Zeit deutlicher und expliziter werden oder unterschiedlichen Rätselteilen Rechnung 	tragen. Im Script sind die Hints bei den Loops festgehalten und jede Staffel mit 	einer fortlaufenden Nummer versehen. Die einzelnen, mittels Schrägstrichen voneinander abgetrennten Hint-Elemente werden meistens zweifach wiederholt, die ganze Einheit als Sprachloop behandelt.

DAS SKRIPT
----------

####CUTSCENE 0 - TUTORIAL

_Die Spieler warten vor der Tür zum Aufführungsraum. Der HOST/die HOSTESSE führt sie in das Konzept ein und betont die Uhrzeit, zu der es losgehen wird._

HOST: Hallo! Schön, dass ihr alle gekommen seid, um 15000 Gray zu spielen. Zuerst ein paar Informationen vorweg. 15000 Gray ist ein Spiel. Das heisst, ihr müsst verschiedene Rätsel lösen, um die Handlung voran zu treiben. Dabei seid ihr ein Team und spielt zusammen - nicht gegeneinander. Tauscht euch aus und sprecht miteinander um die Rätsel zu lösen. So, das wärs auch schon von meiner Seite aus. Um acht geht's los.

####TASK 0 - CHECK IN

_An der Wand hängt eine gehackte [Uhr](../Devices/Uhr), zeigt eine andere Zeit, als die, die der HOST wiederholt nennt. Die Spieler müssen die Uhrzeit verstellen um den Einlass zu triggern._

HOST-LOOP: Um acht geht's los. / Schön, dass ihr alle da seid, um acht geht's los. / Um acht sind wir soweit und die Vorstellung kann beginnen. /

_Die Spieler verstellen die Zeiger der Uhr auf mindestens 20:00, im Bühnenraum geht Licht und Sound an._ 

####CUTSCENE 1 - BOMBE

_Die Spieler betreten das Zimmer. Sie sehen: einen Mann am Boden, dem eine Bombe um den Bauch geschnallt ist. Die Bombe tickt schnell, der Professor stammelt ein letztes "Upsala", dann geht die Bombe in die Luft. Die Assistentinnen betreten rückwärts den Raum, rewind-Geräusch, und spulen sich an ihre Plätze. HANS vollzieht einen Rückwärtsloop „Alaspu" vor sich hin faselnd. Mit dem Ende des Rewinds fallen die Figuren in eine Körperposition im Off-Modus, die Assistentinnen an ihrem jeweiligen Tisch, der Professor an seinen Stuhl gefesselt. Das Licht wechselt._

_Es folgt ein [Radio-Hörspiel](../Sound), Interview mit Hans Hövel, der grade eine bahnbrechende Entdeckung gemacht hat._

####HÖRSPIEL 1 - RADIOBEITRAG

RADIOMODERATOR: Meine sehr verehrten Damen und Herren, ich freue mich außerordentlich, Sie heute wieder zu unserer Reihe »Wissen ist Macht« begrüßen zu dürfen. Ich freue mich jetzt sehr hier Professor Hans Hövel vom Marie Curie Institut für Strahlenforschung begrüßen zu dürfen. Guten Tag, Herr Hövel.

HANS: Guten Tag.

RADIOMODERATOR: Herr Hövel, Sie und Ihr Team forschen seit einigen Jahren an einem Bakterium, das eine ganz besondere Eigenschaft hat.

HANS: Tatsächlich, also, meine beiden Kolleginnen und ich forschen an einem Bakterium, dem Deinococcus Radiodurans. Das ist ein Bakterium, das überlebensfähigste erforschte Lebewesen im Moment, das eine Strahlung - eine radioaktive Strahlung - überlebt, die 150 mal höher sein kann, als die Dosis, die der Mensch überleben würde.

RADIOMODERATOR: Aha, das ist ja hoch interessant, Herr Hövel. Und was genau haben Sie mit dem Bakterium vor?

HANS: Nun, wir versuchen die genetische Struktur und die Reparaturmechanismen dieses Bakteriums auf andere Lebewesen zu übertragen, um dann in einem nächsten Schritt sozusagen…

RADIOMODERATOR: Um in einem nächsten Schritt ebendiese Strahlenresistenz auf den Menschen übertragbar zu machen, richtig? 

HANS: Ja, ja, genau. Also darin bestünde dann das Endziel.

RADIOMODERATOR: Was für Ergebnisse haben Sie denn bisher erzielt?

HANS: Es ist uns wichtig, sorgsam mit den Ergebnissen unserer Forschung umzugehen. Also, die Entwicklung des Verfahrens steht unter strengster Geheimhaltung - ich hoffe, Sie verstehen das! Das hat natürlich auch sehr viel mit sensiblen Informationen zu tun, die wir verwalten müssen. Die können unter Umständen in falsche Hände geraten, was wir nicht wollen.

RADIOMODERATOR: Ha, ha, Herr Professor Hövel, ich kann Ihnen versichern, weder meine Hörer noch ich werden jemandem etwas verraten. Vielen Dank für das Gespräch und viel Erfolg weiterhin.

HANS: Ja, danke auch! Ich hoffe, dass nichts verraten wird. Danke.

_Dann geht das Licht über Monica an, she throws her hands up in despair:_

####CUTSCENE 2 - PROBLEM

MONICA: Oh nein oh nein oh nein oh nein oh nein. Das ist ein Problem. Ein großes Problem. Was macht ich denn jetzt? Der bringt mich um. Ich muss den Boss anrufen. Wo hab ich nochmal die Nummer hingetan?

####TASKSCENE 1 - TELEFONNUMMER

_Die Spieler müssen die richtige Telefonnummer finden und mit dem [Telefon](../Devices/Telefon) anrufen. Bei der Nummerhandelt es sich um die von Jörg vom Segelclub._

MONICA_LOOP1: Wo hab ich nochmal seine Nummer hin getan? / Die muss hier irgendwo sein. / Ich muss den Boss anrufen. /

_Wenn die Spieler die Liste finden variiert MONICA ihren Loop._

MONICA_LOOP2: Seine Nummer muss doch irgendwo auf der Liste sein / Die muss hier irgendwo sein. / Ich muss den Boss anrufen. / Der bringt mich um… / Ich muss JETZT dringend den Boss anrufen. /

_Sollten es die Spieler trotz der gefundenen Liste nicht schaffen den Boss (Jörg vom Segelclub) anzurufen, springt MONICA in den dritten Hint-Loop._

MONICA_LOOP3: Seine Nummer muss doch irgendwo auf der Liste sein / Die muss hier irgendwo sein. / Ich muss den Boss anrufen. / Hoffentlich ist er nicht gerade im Segelurlaub. / Ich muss JETZT dringend den Boss anrufen. /

_Wenn die richtige Nummer gewählt wird - 42237 - springt das Hörspiel 2 an._

####CUTSCENE 3/HÖRSPIEL 2 - JÖRG	

_Monica und Jörg telefonieren._

JÖRG: Hallo? Monica, bist du es? 

MONICA: Hallo, Jörg! Ja, ich - also…

JÖRG: Monica?

MONICA: Ja.

JÖRG: Was ist los?

MONICA: Ich ähm… nichts, es, also es läuft alles nach Plan, sozusagen...

JÖRG: Monica, ich habe keine Lust mehr auf Ausreden. Jetzt hör mir mal gut zu. Hör endlich auf, dich von dem Professor um die Finger wickeln zu lassen. Wir brauchen die Ergebnisse. Jetzt. Er hat gestern Nacht was rausgefunden. Ach so: Ich habe jetzt den PIN bekommen. Er lautet Pi. Ich wiederhole: Pi.

####TASKSCENE 2 - PI

_Die Lichtstimmung wechselt. Das [Num-Pad](../Devices/Numpad) wird beleuchtet. Monica legt auf._

MONICA: Also Pi.

_Versucht auf der Computertastatur Pi einzugeben. Funktioniert nicht. Kein Strom. Rage! Gekreische! Scheiss Maschine. Erstmal nen Kaffee trinken._

MONICA: Oh, Kaffee!

_Kaffee eingießen, dann:_	 	

MONICA_LOOP1: Also Pi… / Am Computer scheint man das irgendwie nicht eingeben zu können. / 

_In ihrem Loop klimpert sie regelmässig mit dem Löffel an die Kaffeetasse. Nach einer Weile springt sie in Loop2._

MONICA_LOOP2: _Da gab es doch neulich dieses Rundschreiben. / Was stand noch mal in diesem Rundschreiben? / Das ging an alle Abteilungen /_

_Sobald die Spieler das Num-Pad entdecken kommentiert MONICA dies und springt in den nächsten Loop._

MONICA_LOOP3: Oh! Wozu war nochmals dieses Kästchen gut? / Da gab es doch neulich dieses Rundschreiben. / Was stand noch mal in diesem Rundschreiben? / Aber Pi ist doch viel länger als nur drei Stellen. / Pi ist ja geradzu undendlich /

_Wenn die Spieler das Rundschreiben gefunden haben (es liegt in der Ausstattung zwischen den Computern versteckt)_ _springt MONICA in die nächste Loop-Variation._

MONICA_LOOP4: _Aber Pi ist doch viel länger als nur drei Stellen. / Der Professor sagt immer „Latein ist das bessere Griechisch" / Also Pi! /_

_Nach einer Weile:_

MONICA_LOOP5: Der Professor sagt immer „Latein ist das bessere Griechisch" / Wie schreibt man gleich nochmal Pi auf Latein? / 

_Nach einer weiteren Weile:_

MONICA_LOOP6: Der Professor sagt immer „Latein ist das bessere Griechisch" / Wie schreibt man gleich nochmals Pi auf Latein? / P, I / Alpha, Beta, Gamma… _Sie zählt mit den Fingern._ /

_Auf dem [Whiteboard](../Ausstattung/Dokumente/Whiteboard) befindet sich eine Tabelle, eine mit Zahlen versehene Übersetzung vom Lateinischen Alphabet ins Griechische. Daneben steht der von MONICA zitierte Satz „Latein ist das bessere Griechisch". Die Spielerinnen tippen den richtigen Code auf dem Num-Pad ein. 169 (P ist die 16. Stelle des Alphabets, I die 9.) Bam. Der Computer geht an, MONICA tippt noch was, dann Swoosh, Fokus auf AMANDA._

_Diese so: Wutausbruch. Ordner runterschmeißen. Attention._

####CUTSCENE 4 - AMANDA

_AMANDAS und HANS' Licht gehen an, MONICAs aus. Erstere läuft rum. MistMist. Professor. Versucht ihn zu wecken, stupst ihn etwas an._

AMANDA: Professor, was soll ich denn jetzt mit dir machen? Ich brauch dich doch noch. Amanda, jetzt reiss dich aber mal zusammen. Wofür hast du denn studiert. Dafür muss es doch ne Lösung geben… Ne Lösung! Zum Beispiel diese hier…

_Reagenzgläser zu AMANDAS Schreibtisch holen. Sie enthalten Flüssigkeiten unterschiedlicher Farben, sowie drei kleinere Behältnisse mit Pulver._

####TASKSCENE 3 - WAKE UP

_Die Spielerinnen müssen ein [Rezept](../Ausstattung/Dokumente/Mischr%C3%A4tsel) vervollständigen, in dem sie ein Zettelchen im Drucker und ein Zettelchen in AMANDAS Notizbuch finden. Die drei Teile zusammengefügt ergeben eine Rezeptur._

AMANDA-LOOP: Wo ist denn jetzt mein Rezept? / Also, irgendwas fehlt da doch noch! /

_Dann müssen sie eine Mische zusammenmixen. Zunächst gilt es eine violette Farbe mit den Flüssigkeiten herzustellen._

AMANDA_LOOP2: Wie geht das gleich nochmal? / Also, da muss doch noch was rein. / Irgendwas fehlt hier doch noch. /

_Blau+Rot = lila = schon mal super, aber blubbern muss es noch. Deswegen braucht es das Brausepulver. Die Fläschchen haben drei Namen: Amphetamin, Serotonin und Koffein. Leider ist auf der Rezeptur nur die chemische Formel versehen. Aber ein Hint gibt es auf der weissen Tafel: Amphetamin: C9H13N. Die Spieler schmeissen alles zusammen und:_

AMANDA: Korrekt! Ich erinnere mich an diesen Blubber. So, Professor, dann wollen wir dich mal wecken, mein Freund.

**Achtung: Wenn unterwegs etwas falsches gemixt wird (falsche Farbe oder falsches Pulver), giesst Amanda das Produkt in den Topf einer verdorrten Zimmerpflanze und kommentiert: „Nein, so war das nicht. Nochmals von vorn."**

####CUTSCENE 5 - WAKE UP

_Wenn die Spielerinnen das Rätsel gelöst haben, flösst Amanda dem Professor den Suff ein. Er so: übelster Hustanfall (MONICAS Liht kommt dazu), wacht langsam auf. Verfällt dann aber parallel zu den beiden Mitarbeiterinnen in einen Benommenheitsloop. Die Assistentinnen schauen sich an, panisch:_

AMANDA und MONICA: Plan B!

####TASKSCENE 4 - PISTOLE

_Beide rennen zu ihren Pistolenverstecken, haben aber offensichtlich vergessen, wo sie die Pistolenkästchen  hingelegt haben. Es ist die Aufgabe der SpielerInnen, die von der Decke zu holen (mit Hilfe eines Stuhls oder eines Besens)._

AMANDA_LOOP1: Ha! / Wo ist den jetzt mein Kästchen hin?

MONICA_LOOP1: Ha! / Wo ist den jetzt mein Kästchen hin?

HANS_LOOP: *stöhn*

_Die Spielerinnen müssen die Pistole finden. Sie ist in einem der beiden Mäusekäfige versteckt._

MONICA_LOOP2: Hä / Das ist ja leer! / Wo hab ich nochmal die scheiss Knarre versteckt?

AMANDA_LOOP2: Das ist ja leer! / Ja toll, das hab ich ja mal wieder echt gut vor mir selber versteckt. 	  	

_Die Spielerinnen müssen die Pistole entweder in MONICAS oder in AMANDAS Box legen - die die Knarre kriegt spielt dann die Böse, die andere die Unschuldige. Beide stellen sich unter ihr jeweiliges Licht._

####CUTSCENE 6 - ER IST WACH

AMANDA: Ha!

PROFESSOR: Was ist aber hier los?

AMANDA: Ich hab alles unter Kontrolle, entspann dich Professor. 

MONICA: Oder entspann dich nicht. Du hast da was…

_Der Professor sieht an sich runter, ach ja, ne Bombe._

MONICA: Es ist ne Bombe, falls du dich wunderst. 	

PROFESSOR: Ja ich wundere mich!

MONICA: _unterbricht ihn_ Ich weiss dass du etwas herausgefunden hast. Wo ist die Maus? Gib mir die Ergebnisse!

PROFESSOR: Woher weisst du von meinem Durchbruch? 

AMANDA: Also es war so, ich komm so zur Tür rein, und du liegst da so, und sie über dir, und ich denke hm, vielleicht sollte ich besser wieder gehen, aber dann denke ich, vielleicht ist auch was passiert, und ich klopf so in den Türrahmen und sag ich komm jetzt rein und da dreht sie sich um und schaut mich an und ich weiss: auf jeden Fall ist was passiert, und dann hab ich mir zum Glück noch schnell die Pistole weggeschnappt. 

PROFESSOR: Ja, ich wundere mich.

AMANDA: Aber es war leider schon zu spät, die Bombe, sie lief schon und sie läuft immer noch. _Zu Monica_ Mach das doch aus, was ist denn plötzlich in dich gefahren? Was willst du überhaupt?

MONICA: _zum Professor _Gib mir die Ergebnisse!_

PROFESSOR: Gib mir den Code!

AMANDA: Was für einen Code? 

MONICA: Die Ergebnisse!

PROFESSOR: Den Code!

AMANDA: Was für einen Code?

MONICA: Es gibt keinen Code.

PROFESSOR: Es gibt immer einen Code.

AMANDA: WAS DENN FÜR EINEN CODE?

PROFESSOR: Von den Bombe.

AMANDA: Ah.

MONICA: Du hast noch xxx Minuten. _(Liest die Minuten vom Display der Bombe ab)_

PROFESSOR: Für wen arbeitest du?

MONICA: Für eine reine Welt, die du nicht mehr erleben wirst. Wir sorgen dafür, dass nach dem finalen Atomkrieg nur noch die übrig bleiben, die es wert sind zu leben!

PROFESSOR: Das Deinococcos Radiodurans kann noch nicht auf den Menschen übertragen werden, nur weil es ein mal bei einer Maus geklappt hat.

MONICA: Das lass unsere Sorge sein. Welche Maus wars, Hövel?

PROFESSOR: Ich weigere mich! Ich lasse es nicht zu, dass ihr meine Ergebnisse für eure Zwecke missbraucht.

AMANDA: _zu Monica_Du hast uns die ganze nur Zeit ausspioniert?_

MONICA: Richtig. _ Zum Professor _Aber weil du so ein Eigenbrödler bist und in den ganzen Jahren keine einzige Information mit mir geteilt hast, muss ich dich jetzt zwingen - Gib mir die Ergebnisse!_

AMANDA: Ich hab ne Knarre. Ich mach dich kalt.

MONICA: Musst du nicht. Wir gehen hier eh gleich alle in die Luft.

HÖVEL: Dann ist es wohl so.

AMANDA: HÄ?!

MONICA: Also mir ist es gleich.

AMANDA: Aber Hans, wenn du wirklich gefunden hast, wonach du so lange gesucht hast, dann könnte das die Menschheit vor viel Übel bewahren. Willst du wirklich, dass es jetzt einfach so mit dir in die Luft geht? Wir tragen Verantwortung. 

HÖVEL: Das leuchtet mir ein.

AMANDA: Pass auf. Ich hab die Pistole. Ich kann fliehen. Sag du mir, was du weißt und ich sorge dafür, dass die Information an die richtigen Leute gelangt.

_Der Professor denkt kurz nach, dann flüstert er AMANDA was ins Ohr._

AMANDA: Also im Systemordner…

####TASKSCENE 5 - RONNY ODER PAULA?

_AMANDA geht zum [Computer](../Devices/Computer), schiebt eine Diskette rein und versucht ihn zu bedienen, nichts klappt._

AMANDA_LOOP1: Och, nö. Wie geht denn nochmal dieser Computer? /

MONICA_LOOP1: Yes! /

HANS_LOOP1: *kuckt blöd zwischen Monica und Amanda hin und her. Wechselt dabei zwischen einem erfreuten und einem misstrauischen Gesichtsausdruck* /

_Die Spielerinnen müssen rausfinden wie der Computer funktioniert. Das steht auf so Post-It-Zettelchen erklärt. Sie müssen auf dem Laufwerk D den Systemordner finden und dort drin die Dokumente "Ronny" und „Paula". Dann müssen sie rausfinden, mit welcher Maus die Versuche geklappt haben, nämlich mit Ronny, und sich entscheiden, welche Datei sie auf die Diskette kopieren. Wenn die Spieler die Navigation einigermassen raus haben:_

AMANDA_LOOP2: Ich muss irgendwie diesen Systemordner finden. / Der muss doch irgendwo auf diesem Computer sein. /

_AMANDA und MONICA schauen sich während des Loops an, strecken ihren Unterarm mit dem Tattoo der Sekte aus und lassen darüber in einer verschwörerischen Geste eine Bombe hoch gehen. Boom._

_Wenn die Spieler den Systemordner gefunden haben:_

AMANDA_LOOP3: Ah! Ich glaube, ich bin auf einem guten Weg! / Wie öffne ich nochmal einen Ordner? /

_Wenn die Spieler den Systemordner geöffnet haben:_

AMANDA_LOOP4: Und für wen soll ich mich jetzt entscheiden? Paula oder Ronny? / Bei welcher Maus haben die Versuche wohl geklappt? / Ich muss mich entscheiden und dann schnell weg hier. / Oh, oh, nur noch xx Minuten… /

####CUTSCENE 7 - DAS ENDE

_Sobald die Datei kopiert ist, schnappt sich Amanda die Diskette, geht zu MONICA, die beiden lachen hysterisch über ihre geglückte Intrige, high five._

AMANDA und MONICA: Tschüss Professor, und vielen Dank. Muahaha!

_Sie rennen raus, lassen vorher aber netterweise ihre weissen Kittel noch an der Garderobe hängen._

HANS: Hey, Amanda! Monica! Hey! Ihr zwei…! Ich..! Ihr könnt mich doch nicht einfach hier..! Hey! Ich..! (…) Upsala! _AMANDAS und MONICAS Lichter gehen aus, einzig der Professor ist noch beleuchtet._

####TASKSCENE 6 - DIE BOMBE

_Die Spielerinnen müssen jetzt in der verbleibenden Zeit die [Bombe](../Devices/Zeitbombe) entschärfen. Den Code finden sie in AMANDAS Kittel. Gemäss der Zeichnung sind die Drähte neu zu arrangieren. Doch Obacht: Ein herausgezogener Draht lässt die Bombe schneller Ticken._

HANS_LOOP1: Upsala! /

HANS:LOOP2: _frei variierend _Uiuiuiuiui, wie die Zeit vergeht/rast! / Upsala! / Mann, oh Mann! Keine X Minuten mehr! / Ob das wohl arg schmerzt, wenn diese Bombe hoch geht? /_

####CUTSCENE 8 WIN - WIN-TANZ

_Sobald die Bombe entschärft wird: Winsound. HANS steht auf, begibt sich in die Mitte des Raums und führt eine roboartigen Win-Tanz auf. Mit dem Ende des Tracks geht das Licht aus und der Professor fällt in sich zusammen (Off-Modus)._

####CUTSCENE 8 LOSE - EXPOLSION

_Die Bombe tickt runter und explodiert. Das Licht geht aus und der Professor fällt in sich zusammen (off-Modus)._

####CUTSCENE 9/HÖRSPIEL 3 - RONNY

_Wenn die Daten von Ronnys Versuch auf die Diskette gespeichert wurden, zeigen die Bildschirme der Rechner einen Atompilz._

JÖRG: Ah, meine Kirschen! Ich wusste, ihr seid Gold wert. Jetzt steht unserer neuen, grossartigen Weltordnung nichts mehr im Wege!

_ENDE! Licht aus. Ciao und tschüss._

####CUTSCENE 9/HÖRSPIEL 3 - PAULA

_Wenn die Daten von Paulas Versuch auf die Diskette gespeichert wurden, zeigen die Bildschirme der Rechner den Text „achievment unlocked: WORLD SAVIOR"._

JÖRG: Ihr armseeligen Dilettanten! Ihr habt die falsche Datei kopiert! Was sollen wir mit dem Mist anfangen? Wartet bis ich euch in die Finger kriege. Euer Leben ist keinen Pfifferling mehr wert.

_ENDE! Licht aus. Tutti Finito._
